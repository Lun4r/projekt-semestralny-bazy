DROP DATABASE IF EXISTS projekt_pomierski; --usuwam bazę danych projekt_pomierski jeśli istnieje

CREATE DATABASE projekt_pomierski CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; --tworze baze danych projekt_pomierski, ustawiam kodowanie znaków na utf8mb4 oraz metodę porówywania napisów na utf8mb4_inicode_ci, aby baza danych obsługiwała polskie znaki

USE projekt_pomierski; --przechodzę do wcześniej utworzonej bazy projekt_pomierski

CREATE TABLE osoby(
id_osoby CHAR(4) PRIMARY KEY,
imie VARCHAR(25) NOT NULL,
nazwisko VARCHAR(35) NOT NULL,
nr_telefonu INT(9)
); --tworzę tabelę osoby spełniającą wymogi pliku tekstowego zawierającego dane


ALTER TABLE osoby CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; --zmieniam kodowanie znaków na utf8mb4 oraz metodę porówywania napisów na utf8mb4_inicode_ci, aby baza danych obsługiwała polskie znaki

CREATE TABLE psy(
id_psa INT(5) PRIMARY KEY,
rasa CHAR(60) NOT NULL,
wiek TINYINT(2) NOT NULL,
plec CHAR(6) NOT NULL,
ilosc_medali TINYINT(2),
id_osoby CHAR(4)
); --tworzę tabelę psy spełniającą wymogi pliku tekstowego zawierającego dane

ALTER TABLE psy CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; --zmieniam kodowanie znaków na utf8mb4 oraz metodę porówywania napisów na utf8mb4_inicode_ci, aby baza danych obsługiwała polskie znaki

ALTER TABLE psy ADD FOREIGN KEY (id_osoby) REFERENCES osoby(id_osoby); --dodaje klucz obcy na pole id_osoby, aby wskazywał on na klucz głowny (id_osoby) tabeli osoby. Tworze dzięki temu relację między dwoma tabelami, którą wykorzystam do wykonania późniejszych zadań.

LOAD DATA LOCAL INFILE 'D:/dane/osoby.txt' INTO TABLE osoby
FIELDS TERMINATED BY ';' 
LINES TERMINATED BY '\r\n'; --Wczytuję dane z pliku osoby.txt znajdującego się pod ścieżką D:/dane/ do tabeli osoby. Informuje o odzieleniu pól przez znak średnika oraz o zakończeniu linii przez enter

LOAD DATA LOCAL INFILE 'D:/dane/psy.txt' INTO TABLE psy
FIELDS TERMINATED BY ';'
LINES TERMINATED BY '\n'
IGNORE 3 ROWS; --Wczytuję dane z pliku psy.txt znajdującego się pod ścieżką D:/dane/ do tabeli psy. Ignoruje 3 pierwsze wiersze (nie znazałem sposobu na zignorowanie 3 ostatnich, tak jak podane było w treści zadania) Informuje o odzieleniu pól przez znak średnika oraz o zakończeniu linii przez enter

INSERT INTO psy VALUES ('1', 'wyzel wegierski krotkowlosy', '4', 'samica','1','o059'); --Dodaje do tabeli psy pierwszy usunięty wcześniej rekord.
INSERT INTO psy VALUES ('2', 'owczarek niemiecki','2','samica','2','o064'); --Dodaje do tabeli psy drugi usunięty wcześniej rekord.
INSERT INTO psy VALUES ('3','chihuahua','3','samiec','4','o097'); --Dodaje do tabeli psy trzeci usunięty wcześniej rekord.

CREATE VIEW V1 AS 
SELECT Psy.plec, Count(Psy.plec) AS Ilosc_psow FROM Psy GROUP BY Psy.plec; --Tworze widok o nazwie V1, który będzie zawierał wyknik kwerendy mającej na celu podać liczbę samców oraz liczbę samic wśród psów

CREATE VIEW V2 AS 
SELECT Osoby.nazwisko, Osoby.imie, Count(Psy.id_psa) AS ilosc_psow
FROM Osoby INNER JOIN Psy ON Osoby.id_osoby = Psy.id_osoby
GROUP BY Osoby.nazwisko, Osoby.imie
HAVING (((Count(Psy.id_psa))>8))
ORDER BY Osoby.nazwisko; /* Tworze widok o nazwie V2, który będzie zawierał wyknik kwerendy mającej na celu utworzenie zestawienia podające nazwiska i imiona osób, które mają więcej niż 8 psów.
Zestawienie powinno być uporządkowane alfabetycznie według nazwisk. */ 

CREATE VIEW V3 AS
SELECT Osoby.imie, Osoby.nazwisko, Sum(Psy.ilosc_medali) AS Suma_medali
FROM Osoby INNER JOIN Psy ON Osoby.id_osoby = Psy.id_osoby
GROUP BY Osoby.imie, Osoby.nazwisko
ORDER BY Sum(Psy.ilosc_medali) DESC LIMIT 1; /* Tworze widok o nazwie V1, który będzie zawierał wyknik kwerendy mającej na celu podać imię i nazwisko osoby, której psy zdobyły łącznie najwięcej medali, oraz podaj
liczbę tych medali. */

CREATE VIEW V4 AS
SELECT Count(Osoby.id_osoby) AS Ilosc_wlascicieli, Psy.rasa
FROM Osoby INNER JOIN Psy ON Osoby.id_osoby = Psy.id_osoby
GROUP BY Psy.rasa
HAVING (((Psy.rasa) LIKE "%_wczarek%")); /* Tworze widok o nazwie V1, który będzie zawierał wyknik kwerendy mającej na celu podać liczbę osób posiadających owczarki. Zwracam uwagę na to, że nazwa rasy może
składać się z kilku wyrazów oraz że jedna osoba może posiadać kilka owczarków tej
samej rasy lub różnych ras. */

mysqldump --databases projekt_pomierski > projekt_pomierski_backup.sql